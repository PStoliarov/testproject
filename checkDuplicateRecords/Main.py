import datetime
import sqlite3
from tkinter import *
from Person import Person
from datetime import *
from PIL import *

root = Tk()
cur_date = datetime.today()
cur_month = datetime.today().month
print(cur_month)
yesterday = cur_date - timedelta(days=1)
# from 0904
day0604 = cur_date - timedelta(days=4)
num_cooldown_days = 4
cooldown_day = cur_date - timedelta(days=num_cooldown_days)
root.title('Черга ' + str(cur_date.date()))
day_to_post_add = cur_date - timedelta(days=2)

# Prepare images
my_img1 = PhotoImage(file="checkmark.png")
image_success = my_img1.subsample(2)
my_img2 = PhotoImage(file="xmark.png")
image_fail = my_img2.subsample(2)

# Open connection to DB
conn = sqlite3.connect('person.db')
cur = conn.cursor()

# UNCOMMENT NEXT LINE AFTER REMOVING DB!!!
# cur.execute("CREATE TABLE persons (person_surname text, person_name text, queue_date text)")

# UNCOMMENT NEXT LINE TO FILL IN THE DATABASE VIA CODE!!!

'''
for i in range(len(list_to_add)):
    cur.execute("INSERT INTO persons VALUES (:l_name, :f_name, :cur_date)", {'l_name': list_to_add[i][0], 'f_name': list_to_add[i][1], 'cur_date': list_to_add[i][2]})

conn.commit()

'''
# cur.execute("INSERT INTO persons VALUES ('Doe', 'John')")
list_to_add = [
    ("Макарец", "Михайло", str(day_to_post_add.date())),
("Малюга", "Наталія", str(day_to_post_add.date())),
("Малюга", "Альона", str(day_to_post_add.date())),
("Малюга", "Володимир", str(day_to_post_add.date())),
("Малюга", "Анжеліка", str(day_to_post_add.date())),
("Гегухенька", "Людмила", str(day_to_post_add.date())),
("Гегухенький", "Роман", str(day_to_post_add.date())),
("Вітченко", "Вікторія", str(day_to_post_add.date())),
("Зюзя", "Сергій", str(day_to_post_add.date())),
("Сирчина", "Лариса", str(day_to_post_add.date())),
("Сирчина", "Андрій", str(day_to_post_add.date())),
("Ширай", "Світлана", str(day_to_post_add.date())),
("Ширай", "Данііл", str(day_to_post_add.date())),
("Симоненко", "Оксана", str(day_to_post_add.date())),
("Симоненко", "Владислав", str(day_to_post_add.date())),
("Буренок", "Любов", str(day_to_post_add.date())),
("Симоненко", "Петро", str(day_to_post_add.date())),
("Коваленко", "Поліна", str(day_to_post_add.date())),
("Коваленко", "Оксана", str(day_to_post_add.date())),
("Коваленко", "Андрій", str(day_to_post_add.date())),
("Швець", "Тетяна", str(day_to_post_add.date())),
("Швець", "Микола", str(day_to_post_add.date())),
("Попутько", "Надія", str(day_to_post_add.date())),
("Попутько", "Валерій", str(day_to_post_add.date())),
("Грищенко", "Єгор", str(day_to_post_add.date())),
("Грищенко", "Данііл", str(day_to_post_add.date())),
("Грищенко", "Наталія", str(day_to_post_add.date())),
("Грищенко", "Данііл", str(day_to_post_add.date())),
("Руднік", "Оксана", str(day_to_post_add.date())),
("Руднік", "Олексій", str(day_to_post_add.date())),
("Руднік", "Денис", str(day_to_post_add.date())),
("Доценко", "Володимир", str(day_to_post_add.date())),
("Розумєєнко", "Євгеній", str(day_to_post_add.date())),
("Шугалій", "Наталія", str(day_to_post_add.date())),
("Мазко", "Лариса", str(day_to_post_add.date())),
("Копрал", "Ольга", str(day_to_post_add.date())),
("Ховрий", "Наталія", str(day_to_post_add.date())),
("Ленько", "Віталій", str(day_to_post_add.date())),
("Батюк", "Валентина", str(day_to_post_add.date())),
("Дорошко", "Олена", str(day_to_post_add.date())),
("Негодяєва", "Наталія", str(day_to_post_add.date())),
("Біпун", "Олена", str(day_to_post_add.date())),
("Пащенко", "Оксана", str(day_to_post_add.date())),
("Цупікова", "Альона", str(day_to_post_add.date())),
("Мехед", "Валентина", str(day_to_post_add.date())),
("Безіл", "Юрій", str(day_to_post_add.date())),
("Козицький", "Павло", str(day_to_post_add.date())),
("Козицька", "Ольга", str(day_to_post_add.date())),
("Данільченко", "Ганна", str(day_to_post_add.date())),
("Сирновець", "Марія", str(day_to_post_add.date())),
("Пархоменко", "Ніна", str(day_to_post_add.date())),
("Максименко", "Наталія", str(day_to_post_add.date())),
("Максименко", "Денис", str(day_to_post_add.date())),
("Котеленець", "Наталія", str(day_to_post_add.date())),
("Атрощенко", "Ігор", str(day_to_post_add.date())),
("Сотник", "Гаплина", str(day_to_post_add.date()))
]


# for i in range(len(list_to_add)):
#     cur.execute("INSERT INTO persons VALUES (:l_name, :f_name, :cur_date)", {'l_name': list_to_add[i][0], 'f_name': list_to_add[i][1], 'cur_date': list_to_add[i][2]})
#
# conn.commit()


def submit():
    last_name = l_name.get()
    first_name = f_name.get()
    if last_name != "" and first_name != "":
        # Check if person already was in queue yesterday or today
        cur.execute("SELECT * FROM persons WHERE person_surname=(:person_surname)", {'person_surname': last_name})
        records = cur.fetchall()
        for record in records:
            if record[0] == last_name and record[1] == first_name:
                for i in range(num_cooldown_days):
                    day_to_check = cur_date - timedelta(days=i+1)
                    if record[2] == str(cur_date.date()) or record[2] == str(day_to_check.date()):
                        print("Вже отримав/отримала допомогу: " + str(day_to_check.date()))

                        status_label.config(image=image_fail)
                        return

        try:
            cur.execute("INSERT INTO persons VALUES (:l_name, :f_name, :cur_date)", {'l_name': l_name.get(), 'f_name': f_name.get(), 'cur_date': str(cur_date.date())})

            conn.commit()

            l_name.delete(0, END)
            f_name.delete(0, END)
            l_name.focus_set()
            status_label.config(image=image_success)
        except:
            print("Щось пішло не так...")
            status_label.config(image=image_fail)
    else:
        print("Пусте прізвище або ім\'я")
        status_label.config(image=image_fail)


def on_adding(event):
    submit()


def query():
    # cur.execute("SELECT oid, * from persons WHERE ")
    cur.execute("SELECT oid, * from persons")
    records = cur.fetchall()

    list_viewer = Tk()
    list_viewer.title("Список відвідувачів")
    list_viewer.geometry("500x600")

    # Create scroll bar
    main_frame = Frame(list_viewer)
    main_frame.pack(fill=BOTH, expand=1)

    my_canvas = Canvas(main_frame)
    my_canvas.pack(side=LEFT, fill=BOTH, expand=1)

    sb = Scrollbar(main_frame, orient=VERTICAL, command=my_canvas.yview)
    sb.pack(side=RIGHT, fill=Y)
    my_canvas.config(yscrollcommand=sb.set)
    my_canvas.bind('<Configure>', lambda e: my_canvas.configure(scrollregion=my_canvas.bbox("all")))

    second_frame = Frame(my_canvas)
    my_canvas.create_window((0, 0), window=second_frame, anchor="nw")


    # Create text box labels
    font_header = "Verdana 12 bold"
    font_row = "Verdana 9"
    num_label_lv = Label(second_frame, text="Номер", font=font_header)
    num_label_lv.grid(row=0, column=0)

    l_name_label_lv = Label(second_frame, text="Прізвище", font=font_header)
    l_name_label_lv.grid(row=0, column=1)

    f_name_label_lv = Label(second_frame, text="Ім\'я", font=font_header)
    f_name_label_lv.grid(row=0, column=2)

    date_label_lv = Label(second_frame, text="Дата", font=font_header)
    date_label_lv.grid(row=0, column=3)
    # print(records)

    for record in records:
        num_cur_label = 'lname_label' + str(record[0])
        num_cur_label = Label(second_frame, text=record[0], font=font_row)
        num_cur_label.grid(row=record[0] + 1, column=0)

        lname_cur_label = 'lname_label' + str(record[0])
        lname_cur_label = Label(second_frame, text=record[1], font=font_row)
        lname_cur_label.grid(row=record[0]+1, column=1)

        fname_cur_label = 'fname_label' + str(record[0])
        fname_cur_label = Label(second_frame, text=record[2], font=font_row)
        fname_cur_label.grid(row=record[0] + 1, column=2)

        date_cur_label = 'fname_label' + str(record[0])
        date_cur_label = Label(second_frame, text=record[3], font=font_row)
        date_cur_label.grid(row=record[0] + 1, column=3)
        print(str(record[0]) + "\t\t\t\t\t" + str(record[1]) + "\t\t\t\t\t" + str(record[2]) + "\t\t\t\t\t" + str(record[3]))

    conn.commit()


def edit():

    cur.execute("SELECT oid, * from persons")
    records = cur.fetchall()


def on_closing():
    conn.close()


# Main window


# Addition functionality


# Create text boxes
l_name = Entry(root, width=30)
l_name.grid(row=0, column=1, padx=20)

f_name = Entry(root, width=30)
f_name.grid(row=1, column=1)

# Create text box labels
font = "Verdana 12"
l_name_label = Label(root, text="Прізвище", font=font)
l_name_label.grid(row=0, column=0)

f_name_label = Label(root, text="Ім\'я", font=font)
f_name_label.grid(row=1, column=0)

# Create buttons
submit_btn = Button(root, text="Додати запис", command=submit)
submit_btn.grid(row=2, column=0, columnspan=2, pady=10, padx=10, ipadx=120)


# Query functionality (get records)
query_btn = Button(root, text="Отримати список", command=query)
query_btn.grid(row=3, column=0, columnspan=2, pady=10, padx=10, ipadx=108)


# Create status image
status_label = Label(image=image_success)
status_label.grid(row=4, column=0, columnspan=2)

# Bind Enter (Return) key
root.bind('<Return>', on_adding)
#root.protocol("WM_DELETE_WINDOW", on_closing)



# Edit functionality


# Old entries
old_entry_label = Label(root, text="Номер запису", font=font)
old_entry_label.grid(row=5, column=0)

edit_entry_old = Entry(root, width=30)
edit_entry_old.grid(row=5, column=1)


# New entries
new_surname_label = Label(root, text="Нове прізвище", font=font)
new_surname_label.grid(row=6, column=0)

new_name_label = Label(root, text="Нове ім\'я", font=font)
new_name_label.grid(row=7, column=0)

edit_entry_surname_new = Entry(root, width=30, )
edit_entry_surname_new.grid(row=6, column=1)

edit_entry_name_new = Entry(root, width=30)
edit_entry_name_new.grid(row=7, column=1)


# Update button
# update_btn = Button(root, text="Оновити запис", command=edit)
# submit_btn.grid(row=8, column=0, columnspan=2, pady=10, padx=10, ipadx=150)


# cur.close()
# conn.close()
root.mainloop()
