import datetime
import sqlite3
from tkinter import *
from Person import Person
from datetime import *
from PIL import *

root = Tk()
cur_date = datetime.today()
yesterday = cur_date - timedelta(days=1)
root.title('Черга ' + str(cur_date.date()))

# Prepare images
my_img1 = PhotoImage(file="checkmark.png")
image_success = my_img1.subsample(2)
my_img2 = PhotoImage(file="xmark.png")
image_fail = my_img2.subsample(2)

# Open connection to DB
conn = sqlite3.connect('person.db')
# cur = conn.cursor()

# UNCOMMENT NEXT LINE AFTER REMOVING DB!!!
# cur.execute("CREATE TABLE persons (person_surname text, person_name text, queue_date text)")

# UNCOMMENT NEXT LINE TO FILL IN THE DATABASE VIA CODE!!!
# cur.execute("INSERT INTO persons VALUES ('Doe', 'John')")


def submit():
    cur = conn.cursor()
    last_name = l_name.get()
    first_name = f_name.get()
    if last_name != "" and first_name != "":
        # Check if person already was in queue yesterday or today
        cur.execute("SELECT * FROM persons WHERE person_surname=(:person_surname)", {'person_surname': last_name})
        records = cur.fetchall()
        for record in records:
            if record[0] == last_name and record[1] == first_name:
                if record[2] == str(cur_date.date()) or record[2] == str(yesterday.date()):
                    print("Вже отримав/отримала допомогу")

                    status_label.config(image=image_fail)
                    return

        try:
            cur.execute("INSERT INTO persons VALUES (:l_name, :f_name, :cur_date)", {'l_name': l_name.get(), 'f_name': f_name.get(), 'cur_date': str(cur_date.date())})

            conn.commit()

            l_name.delete(0, END)
            f_name.delete(0, END)
            status_label.config(image=image_success)
        except:
            print("Щось пішло не так...")
            status_label.config(image=image_fail)
    else:
        print("Пусте прізвище або ім\'я")
        status_label.config(image=image_fail)

def on_adding(event):
    submit()

def query():
    conn = sqlite3.connect('person.db')
    cur = conn.cursor()

    cur.execute("SELECT oid, * from persons")
    records = cur.fetchall()

    list_viewer = Tk()
    list_viewer.title("Список відвідувачів")
    list_viewer.geometry("400x100")

    # Create scroll bar
    main_frame = Frame(list_viewer)
    main_frame.pack(fill=BOTH, expand=1)

    my_canvas = Canvas(main_frame)
    my_canvas.pack(side=LEFT, fill=BOTH, expand=1)

    sb = Scrollbar(main_frame, orient=VERTICAL, command=my_canvas.yview)
    sb.pack(side=RIGHT, fill=Y)
    my_canvas.config(yscrollcommand=sb.set)
    my_canvas.bind('<Configure>', lambda e: my_canvas.configure(scrollregion=my_canvas.bbox("all")))

    second_frame = Frame(my_canvas)
    my_canvas.create_window((0, 0), window=second_frame, anchor="nw")


    # Create text box labels
    font_header = "Verdana 12 bold"
    font_row = "Verdana 9"
    num_label_lv = Label(second_frame, text="Номер", font=font_header)
    num_label_lv.grid(row=0, column=0)

    l_name_label_lv = Label(second_frame, text="Прізвище", font=font_header)
    l_name_label_lv.grid(row=0, column=1)

    f_name_label_lv = Label(second_frame, text="Ім\'я", font=font_header)
    f_name_label_lv.grid(row=0, column=2)

    date_label_lv = Label(second_frame, text="Дата", font=font_header)
    date_label_lv.grid(row=0, column=3)
    print(records)

    for record in records:
        num_cur_label = 'lname_label' + str(record[0])
        num_cur_label = Label(second_frame, text=record[0], font=font_row)
        num_cur_label.grid(row=record[0] + 1, column=0)

        lname_cur_label = 'lname_label' + str(record[0])
        lname_cur_label = Label(second_frame, text=record[1], font=font_row)
        lname_cur_label.grid(row=record[0]+1, column=1)

        fname_cur_label = 'fname_label' + str(record[0])
        fname_cur_label = Label(second_frame, text=record[2], font=font_row)
        fname_cur_label.grid(row=record[0] + 1, column=2)

        date_cur_label = 'fname_label' + str(record[0])
        date_cur_label = Label(second_frame, text=record[3], font=font_row)
        date_cur_label.grid(row=record[0] + 1, column=3)

    conn.commit()

def on_closing():
    conn.close()


#

# Create text boxes
l_name = Entry(root, width=30)
l_name.grid(row=0, column=1, padx=20)

f_name = Entry(root, width=30)
f_name.grid(row=1, column=1)

# Create text box labels
font = "Verdana 14"
l_name_label = Label(root, text="Прізвище", font=font)
l_name_label.grid(row=0, column=0)

f_name_label = Label(root, text="Ім\'я", font=font)
f_name_label.grid(row=1, column=0)

# Create buttons
submit_btn = Button(root, text="Додати запис", command=submit)
submit_btn.grid(row=2, column=0, columnspan=2, pady=10, padx=10, ipadx=120)

query_btn = Button(root, text="Отримати список", command=query)
query_btn.grid(row=3, column=0, columnspan=2, pady=10, padx=10, ipadx=108)

# Create status image

status_label = Label(image=image_success)
status_label.grid(row=4, column=0, columnspan=2)

root.bind('<Return>', on_adding)
root.protocol("WM_DELETE_WINDOW", on_closing)
# cur.close()
# conn.close()
root.mainloop()
