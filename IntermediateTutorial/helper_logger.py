import logging
# 1=========================================================================
'''
logger = logging.getLogger(__name__)
logger.propagate = False
#name = logger.name
#print(name)
logger.info('hello from helper')
'''


# 2=========================================================================
'''
logger = logging.getLogger(__name__)
stream_handler = logging.StreamHandler()
file_handler = logging.FileHandler('file.log')

# level and format
stream_handler.setLevel(logging.WARNING)
file_handler.setLevel(logging.ERROR)

formatter = logging.Formatter('%(name)s - %(lineno)s: %(levelname)s - %(message)s')
stream_handler.setFormatter(fmt=formatter)
file_handler.setFormatter(fmt=formatter)

# add handlers
logger.addHandler(hdlr=stream_handler)
logger.addHandler(hdlr=file_handler)


logger.warning(msg='this is an error')
logger.error(msg='this is an error')
'''

# 3=========================================================================
'''
import logging.config
logging.config.fileConfig('logging.conf')

logger = logging.getLogger('simpleExample')
logger.debug('this is a debug message')
'''

# 4=========================================================================
'''
try:
    a = [1,2,3]
    val = a[4]
except IndexError as e:
    #print(e)
    logging.error(e, exc_info=True)
    
'''

# 5=========================================================================
'''
import traceback

try:
    a = [1,2,3]
    val = a[4]
except Exception:
    #print(e)
    logging.error('Exception is: %s', traceback.format_exc())

'''

# 6=========================================================================
'''
from logging.handlers import RotatingFileHandler

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

handler = RotatingFileHandler('logfilename.log', maxBytes=2000, backupCount=5)
logger.addHandler(hdlr=handler)

for _ in range(1000):
    logger.info('hello world!' + str(_))

'''

# 7=========================================================================
'''
from logging.handlers import TimedRotatingFileHandler
import time

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

handler = TimedRotatingFileHandler(filename='timedFile.log', when='s', interval=5, backupCount=5)
logger.addHandler(hdlr=handler)

for _ in range(6):
    logger.info('hello world!' + str(_))
    time.sleep(5)

'''
