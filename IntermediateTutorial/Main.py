#Lists
'''
my_list = ["Cherry", "Banana", "Apple"]
print(len(my_list))

my_list.append("Lemon")
my_list.insert(1, "Orange")
item = my_list.pop()
print(item)
my_list.remove("Banana")
#my_list.clear()
my_list.reverse()
print(my_list)
my_list.sort()
print(my_list)
new_list = sorted(my_list)


list1 = [0] * 5
list2 = [1, 2, 3, 4, 5]
concatList = list1 + list2
print(concatList)


myList = ["Cherry", "Banana", "Apple"]
print(myList)

myList2 = [5, True, "Orange"]
print(myList2)

item = myList[0]
print(item)

for i in myList:
    print(i)

if "Cherry" in myList:
    print("Item is in the list")
else:
    print("Item is NOT in the list")

print(len(myList))
myNumList = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
print(myNumList[:3])
print(myNumList[1::2])
inv = myNumList[::-1]
print(inv[::2])


myListCpy = myList
print(myListCpy)
myListCpy.append("Lemon")
print(myListCpy)
#original list is modified
print(myList)

myListCpy2 = list(myList)
# OR  myListCpy2 = myList[:]
myListCpy2.pop()
print(myListCpy2)
#original list is NOT modified
print(myList)

#squared list
sqrtList = [i*i for i in myNumList]
print(myNumList)
print(sqrtList)
'''


#==================================================================================================================


#Tuples
'''
mytuple = ("Max", 28, "Boston")
print(mytuple)
print(type(mytuple))

mytuple1 = tuple(["Max", 28, "Boston"])
item = mytuple1[0]
# Error in the next line
# mytuple1[0] = "Tim"

for i in mytuple:
    print(i)
name, age, city = mytuple
print(name)
print(age)
print(city)

mytuple2 = ['a', 'p', 'p', 'l', 'e']
print(mytuple2.count('p'))
print(len(mytuple2))
print(mytuple2.index('l'))

my_list = list(mytuple)
mytuple3 = tuple(my_list)

mynumbertuple1 = [1, 2, 3, 4, 5, 6, 7, 8, 9]
mynumbertuple2 = mynumbertuple1[2:5]
print(mynumbertuple2)

i1, *i2, i3 = mynumbertuple1
print(i1)
print(i3)
print(i2)

import timeit
print(timeit.timeit(stmt="[0, 1, 2, 3, 4, 5]", number=1000000))
print(timeit.timeit(stmt="(0, 1, 2, 3, 4, 5)", number=1000000))

import sys
list3 = [0, 1, 2, "hello", True]
my_tuple = (0, 1, 2, "hello", True)

print(sys.getsizeof(list3), "bytes")
print(sys.getsizeof(my_tuple), "bytes")

'''

#==================================================================================================================


#Dictionaries

'''
mydict = {"name": "Max", "age": 28, "city": "New York"}
print(mydict)

mydict2 = dict(name="Mary", age=27, city="Boston")
print(mydict2)

value = mydict["name"]
print(value)
# KeyError
# value2 = mydict["lastname"]

mydict['email'] = "max@xyz.com"
print(mydict)

# override
mydict['email'] = "coolmax@xyz.com"
print(mydict)

# delete or pop
del mydict["name"]
print(mydict)

mydict.pop("age")
print(mydict)

mydict.popitem()
print(mydict)

if "city" in mydict:
    print(mydict["city"])

for key, val in mydict2.items():
    print(key, val)

# mydict_cpy = mydict2
# mydict_cpy["email"] = "maxfax@email.com"
# # copied dict is modified via operator=
# print(mydict_cpy)

mydict_cpy = mydict2.copy()
# OR mydict_cpy = dict(mydict2)
mydict_cpy["email"] = "maxfax@email.com"
print(mydict_cpy)
print(mydict2)

mydict3 = {'name': "Max", 'age': 28, 'email': "maxfax@email.com"}
mydict4 = dict(name="Mary", age=27, city="Boston")

mydict3.update(mydict4)
print(mydict3)

mydict5 = {3: 9, 6: 36, 9: 81}
print(mydict5[3])  # 9

'''


#==================================================================================================================


#Sets

'''
myset = {1, 2, 3}
print(myset)

myset1 = {"Hello"}
print(myset1)
myset2 = set("Hello")
print(myset2)
myset3 = set()
myset3.add(1)
myset3.add(2)
myset3.add(3)

myset3.remove(1)
myset3.discard(4)  # No error if no key found

print(myset3)

for x in myset3:
    print(x)

odds = {1, 3, 5, 7, 9}
evens = {0, 2, 4, 6, 8}
primes = {2, 3, 5, 7}

u = odds.union(evens)
print(u)

i = odds.intersection(evens)
print(i)

setA = {1, 2, 3, 4, 5, 6, 7, 8, 9}
setB = {1, 2, 3, 10, 11, 12}

diff = setA.difference(setB)
print(diff)

diff2 = setA.symmetric_difference(setB)
print(diff2)

setA.update(setB)
print(setA)

setC = {1, 2, 3, 4, 5, 6}
setD = {1, 2, 3}

print(setD.issubset(setC))
print(setC.issuperset(setD))

setE = setC.copy()
setE.add(7)
print(setE)
print(setC)

setF = frozenset([1, 2, 3, 4])
print(setF)

'''


#Strings==================================================================================================================


'''
my_string = """Hello
World"""
print(my_string)

my_string2 = """Hello \
World"""
print(my_string2)

my_string3 = "Hello World"
char = my_string3[0]
print(char)

# strings are immutable, next line = error
# my_string3[0] = 'h'

substring = my_string3[1:5]
print(substring)

inverted_string = my_string3[::-1]
print(inverted_string)

greetings = "Hello"
name = "Tom"
sentence = greetings + " " + name
print(sentence)

for letter in greetings:
    print(letter)

if 'ell' in greetings:
    print("Yes")

my_string4 = "       Hello World         "
my_string4 = my_string4.strip()
print(my_string4)
print(my_string4.upper())

print(my_string4.startswith('Hello'))
print(my_string4.endswith('rld'))

print(my_string4.find('o'))
print(my_string4.find('lo'))
print(my_string4.count('o'))
print(my_string4.replace('Hello', 'Greetings'))
list_of_words = my_string4.split()
print(list_of_words)

my_string5 = "How,are,you,doing"
list_of_words2 = my_string5.split(",")
print(list_of_words2)
new_string = ' '.join(list_of_words2)
print(new_string)

from timeit import default_timer as timer
my_list = ['a'] * 100000
# bad
start1 = timer()
my_string_bad = ''
for i in my_list:
    my_string_bad += i
stop1 = timer()
# print(my_string_bad)
print(stop1-start1)

# good
start2 = timer()
my_string_good = ''.join(my_list)
stop2 = timer()
# print(my_string_good)
print(stop2-start2)

var = "Tom"
my_string6 = "The variable is %s" % var
print(my_string6)
var = 3
my_string7 = "The variable is %d" % var
print(my_string7)
var = 3.123567
my_string8 = "The variable is %.2f" % var
print(my_string8)

var = 3.123567
var2 = 6
my_string9 = "The variable is {:.2f} and {}".format(var, var2)
print(my_string9)

my_string10 = f"The variable is {var*2} and {var2}"
print(my_string10)

'''


#==================================================================================================================


# Collections
'''
from collections import Counter
str_1 = "aaaaaaaabbbbccc"

my_counter = Counter(str_1)
print(my_counter.items())
print(my_counter.most_common(2))
print(my_counter.most_common(1))
print(my_counter.most_common(1)[0])
print(my_counter.most_common(1)[0][0])

print(list(my_counter.elements()))


from collections import namedtuple
Point = namedtuple('Point', 'x, y')
pt = Point(1, 5)
print(pt)


from collections import OrderedDict
ordered_dict = OrderedDict()
ordered_dict['b'] = 2
ordered_dict['c'] = 3
ordered_dict['d'] = 4
ordered_dict['a'] = 1
print(ordered_dict)


from collections import defaultdict
def_dict = defaultdict(float)
def_dict['a'] = 1
def_dict['b'] = 2
print(def_dict)

print(def_dict['a'])
print(def_dict['b'])
print(def_dict['c'])


from collections import deque
print("Deque")
deq = deque()
deq.append(1)
deq.append(2)

deq.appendleft(3)
print(deq)
a = deq.pop()
print(a)
print(deq)

deq.extend([4, 5, 6])
print(deq)
deq.extendleft([99])
deq.rotate(2) #shifts 2 elements in the deque to the right
print(deq)
deq.rotate(-3) #shifts 3 elements in the deque to the left
print(deq)
deq.clear()
'''


#==================================================================================================================
#itertools

'''
#itertools: product, permutations, combinations, accumulate, groupby, and infinite iterators
from itertools import product
a = [1, 2]
b = [3]
prod = product(a, b)
print(prod)
print("Product:")
print(list(prod))

from itertools import permutations
a1 = [1, 2, 3]
perm = permutations(a1)
print("Permutations:")
print(list(perm))


from itertools import combinations, combinations_with_replacement
a2 = [1, 2, 3, 4]
comb = combinations(a2, r=2)
print("Combinations:")
print(list(comb))

comb_with_replacement = combinations_with_replacement(a2, r=2)
print("Combinations with replacement:")
print(list(comb_with_replacement))

from itertools import accumulate
a3 = [1, 2, 3, 4]
accum = accumulate(a3) # i-th elem = sum of all previous elements + current
print("Accumulated list:")
print(list(accum))
import operator

accum2 = accumulate(a3, func=operator.mul)
print("Accumulated list, multiplied:")
print(list(accum2))

a4 = [1, 2, 7, 3, 4]
accum3 = accumulate(a3, func=max) # if each i-th elem of original list is greater than current max elem, it becomes new current max
print("Accumulated list, max:")
print(list(accum3))

from itertools import groupby

def comparator_func(x):
    return x < 3

a5 = [1, 2, 3, 4]
group_obj = groupby(a5, key=comparator_func)
print("Group by function:")
for key1, value1 in group_obj:
    print(key1, value1)
    print(key1, list(value1))

group_obj2 = groupby(a5, key=lambda x: x < 3)
print("Group by lambda function:")
for key2, value2 in group_obj2:
    print(key2, value2)
    print(key2, list(value2))

persons = [{'name': 'Tim', 'age': 16}, {'name': 'Jerry', 'age': 45}, {'name': 'Mike', 'age': 16}, {'name': 'Jane', 'age': 29}]
group_obj3 = groupby(persons, lambda x: x['age'])
print("Group by lambda function (groups by age):")
for key3, value3 in group_obj3:
    print(key3, list(value3))


from itertools import cycle, count, repeat
print('Count from 20:')
for i in count(20):
    print(i)
    if i == 25:
        break


a6 = [1, 2, 3]

# Infinite cycle in for loop below:
# for i in cycle(a6):
#     print(i)

print('Repeat:')
for i in repeat(a6, times=3):
    print(i)

'''


#==================================================================================================================
#Lambda


'''
# Lambda functions
add10 = lambda x: x + 10
sum = add10(5)
print(sum)

mult = lambda x, y: x * y
result = mult(2, 5)
print(result)


points2D = [(1, 2), (15, 1), (5, -11), (10, 3)]
points2D_sortedByY = sorted(points2D, key=lambda x: x[1]) # sort by Y coordinate
points2D_sortedBySum = sorted(points2D, key=lambda x: x[0] + x[1]) # sort by Y coordinate


print(points2D)
print(points2D_sortedByY)
print(points2D_sortedBySum)


# map(func, seq)
a = [1, 2, 3, 4, 5]
b = map(lambda x: x*2, a)
print(list(b))
c = [x*2 for x in a]
print(c)

# filter(func, seq)
a1 = [1, 2, 3, 4, 5, 6]
b1 = filter(lambda x: x%2==0, a1)
print(list(b1))
c1 = [x for x in a1 if x%2==0]
print(c1)

# reduce(func, seq)
from functools import reduce
a3 = [1, 2, 3]
product_a3 = reduce(lambda x, y: x*y, a3)
print(product_a3)

'''


#==================================================================================================================
#Exceptions


'''
# a = 5 + '10' # TypeError
#import somemodule # ModuleNotFoundError
# b = c # NameError
# f = open('somefile.txt') # FileNotFoundError
# a1 = [1, 2, 3]
# a1.remove(4) # ValueError
# value = a1[4] # IndexError
# my_dictionary = {'name': 'Max'}
# my_dictionary['age'] # KeyError

# x = -5
# if x < 0:
#     raise Exception('x should be positive')

#assert (x >= 0), 'x is not positive' # AssertionError



# a = 5/0 # ZeroDivisionError
# try:
#     b = 5/0
#     c = b + '10'
# except ZeroDivisionError as e:
#     print('an error occurred:')
#     print(e)
# except TypeError as e:
#     print('an error occurred:')
#     print(e)
# else:
#     print('no exceptions raised')
# finally:
#     print('this message is shown regardless whether an exception is raised or not')




class ValueToHighError(Exception):
    pass

class ValueToLowError(Exception):
    def __init__(self, message, value):
        self.message = message
        self.value = value



def test_value(x):
    if x > 100:
        raise ValueToHighError('value is too high')
    if x < 0:
        raise ValueToLowError('value is too low', x)

try:
    test_value(-1)
except ValueToHighError as e:
    print(e)
except ValueToLowError as e:
    print(e.message, e.value)
'''


#==================================================================================================================
# Logging

import logging
# logging.basicConfig(level=logging.DEBUG, format='At logger \"%(name)s\", %(lineno)s: %(asctime)s - %(levelname)s - %(message)s',
#                     datefmt='%d/%m/%Y %H:%M:%S')
# logging.debug('This is a debug message')
# logging.info('This is an info message')
# logging.warning('This is a warning message')
# logging.error('This is an error message')
# logging.critical('This is a critical message')

import helper_logger
