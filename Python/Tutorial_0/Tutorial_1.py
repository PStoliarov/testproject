#Tuples: ordered, immutable, allows duplicate elements

mytuple = ("Max", 28, "Boston")
#mytuple = "Max", 28, "Boston"  # also possible
mytuple1 = tuple(["Max", 28, "Boston"]) # create tuple from iterable
print (mytuple)

tupletype =("Max",) #put comma in the end if tuple only has 1 element. Otherwise python doesn't recognize the whole element as tuple
print(type(tupletype))

item = mytuple[-1]
print(item)

#mytuple[0] = "Tim" # TypeError: 'tuple' object does not support item assignment

for i in mytuple:
    print(i)


letters_tuple = ('a', 'p', 'p', 'l', 'e')
print(len(letters_tuple)) #5
print(letters_tuple.count('p')) #2

print(letters_tuple.index('p')) #1  #ValueError if no index found for element  

my_list = list(mytuple) #conversion
mytuple2 = tuple(my_list) #conversion

tuple_numbers = (1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
tuple_numbers2 = tuple_numbers[2:5]
print(tuple_numbers2)
tuple_numbers3 = tuple_numbers[::2] # take every second element
print(tuple_numbers3)
tuple_numbers4 = tuple_numbers[::-1] # inverse order
print(tuple_numbers4)

my_tuple = "Max", 28, "Boston"
name, age, city = my_tuple # unpacking, number of elements must match

my_tuple_numbers = (0, 1, 2, 3, 4)

i1, *i2, i3 = my_tuple_numbers
print(i1) #0
print(i3) #4
print(i2) #list: [1, 2, 3]

import sys
my_list1 = [0, 1, 2, "hello", True]
my_tuple1 = (0, 1, 2, "hello", True)
print(sys.getsizeof(my_list1), "bytes") # list is larger
print(sys.getsizeof(my_tuple1), "bytes")


import timeit
print(timeit.timeit(stmt="[0, 1, 2, 3, 4, 5]", number=1000000)) # statement, repeat number of times
print(timeit.timeit(stmt="(0, 1, 2, 3, 4, 5)", number=1000000))