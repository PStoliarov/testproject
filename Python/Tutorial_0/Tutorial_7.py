# lambda arguments: expression
add10 = lambda x: x + 10 # add10 is now a function
print(add10(5))

mult = lambda x,y: x*y
print(mult(2, 7))


points2D = [(1, 2), (15, 1), (5, -1), (10, 4)]

def sort_by_y(x):
    return x[1]

points2D_sorted = sorted(points2D, key=lambda x: x[1])
#points2D_sorted = sorted(points2D, key=sort_by_y) # produces the same result
print(points2D)
print(points2D_sorted)

points2D_sorted_by_sum = sorted(points2D, key=lambda x: x[0] + x[1])
print(points2D_sorted_by_sum)

# map(func, seq)
list_a = [1, 2, 3, 4, 5, 6]
map_b = map(lambda x: x*2, list_a)
print(list(map_b))
list_b = [x*2 for x in list_a]
print(list_b)

# filter
list_filtered = filter(lambda x: x%2==0, list_a)
print(list(list_filtered))

list_c_bool = [x%2==0 for x in list_a] # gets list of Boolean values
print(list_c_bool)

list_c_num = [x for x in list_a if x%2==0]
print(list_c_num)

# reduce
from functools import reduce
list1 = [1, 2, 3, 4]

product_a = reduce(lambda x, y: x*y, list1)
print(product_a)