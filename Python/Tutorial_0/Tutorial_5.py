# Collections: Counter, namedtuple, OrderedDict, defaultdict, deque
from collections import Counter

str_a = "aaaaaaabbbbbccc"
my_counter = Counter(str_a)
print(my_counter)
print(my_counter.items())
print(my_counter.keys())
print(my_counter.values())
print(my_counter.most_common(2)) #get list of 2 tuples with the most common occurances
print(my_counter.most_common(2)[0][0]) #get list of 2 tuples with the most common occurances, access 1st tuple, access key

print(list(my_counter.elements()))


from collections import namedtuple
Point = namedtuple('Point', 'x, y')
pt = Point(1, -4)
print(pt)
print(pt.x, pt.y)


from collections import OrderedDict
ordered_dict = OrderedDict()
ordered_dict['b'] = 2
ordered_dict['c'] = 3
ordered_dict['d'] = 4
ordered_dict['a'] = 1

print(ordered_dict)


from collections import defaultdict
d = defaultdict(int)
d['a'] = 1
d['b'] = 2
print(d)
print(d['a']) # 1
print(d['c']) # entry with key 'c' does not exist, default int value is assigned


from collections import deque
deq= deque()
deq.append(1)
deq.append(2)

deq.appendleft(3)
print(deq)
deq.pop()
print(deq)

deq.popleft()
print(deq)

deq.extend([4, 5, 6])
deq.extendleft([0, -1, -2])
print(deq)

deq.rotate(1) # rotate each element one position to the right
deq.rotate(-2) # rotate each element two position to the left