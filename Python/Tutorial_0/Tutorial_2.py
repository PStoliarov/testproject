# Dictionary: Key-Value pairs, Unordered, Mutable
mydict = {"name": "Max", "age": 28, "city": "New York"}
print(mydict)

mydict2 = dict(name="Mary", age=27, city="Boston")
print(mydict2)

value = mydict["name"] #KeyError if key is not in dictionary
print(value)

mydict["email"] = "max@xyq.com"
print(mydict)

del mydict["name"]
print(mydict)

mydict.pop("age")
print(mydict)

mydict.popitem() #since python 3.7 removes last inserted item
print(mydict)


if "name" in mydict2:
    print(mydict2["name"])

for key in mydict2.keys():
    print(key)

for value in mydict2.values():
    print(value)

for key, value in mydict2.items():
    print(key, ": ", value)


#mydict_cpy = mydict2 # wrong, original dict can get modified if changing mydict_cpy
mydict_cpy = mydict2.copy() # or mydict_cpy = dict(mydict2)
print(mydict_cpy)

mydict_cpy["email"] = "max@xyq.com"
print(mydict2)


dict1 = {"name": "Max", "age": 28, "email": "max@xyq.com"}
dict2 = dict(name="Mary", age=27, city="Boston")

dict1.update(dict2)
print(dict1)


dict_squared = {3: 9, 6: 36, 9: 81}
print(dict_squared)

value = dict_squared[3]
print(value)


my_tuple = (8, 7)
dict_mixed = {my_tuple: 15} # dict can contain any immutable types, e.g. tuple
print(dict_mixed)