# Logging

import logging
import logging.config
logging.debug('This is a debug message')        # This is not printed by default
logging.info('This is an info message')         # This is not printed by default
logging.warning('This is a warning message')    # This is printed by default
logging.error('This is an error message')       # This is printed by default
logging.critical('This is a critical message')  # This is printed by default

print('Changing configs...')

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    datefmt='%m/%d/%Y %H:%M:%S', force=True)

logging.debug('This is a debug message')
logging.info('This is an info message')
logging.warning('This is a warning message')
logging.error('This is an error message')
logging.critical('This is a critical message')

# Creating own logger to use at specific module, before a root logger was used
logger = logging.getLogger(name=__name__)
logger.info('Logger name is ' + logger.name)
logger.propagate = False
logger.info('This message is not propagated to base logger...') # Messages with warning level and higher are propagated for some reason


print('---------------------------------------------------------')


# create handler
stream_h = logging.StreamHandler()
file_h = logging.FileHandler('file.log')
# level and format
stream_h.setLevel(level=logging.WARNING)
file_h.setLevel(level=logging.ERROR)
formatter = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
stream_h.setFormatter(formatter)
file_h.setFormatter(formatter)

logger.addHandler(stream_h)
logger.addHandler(file_h)

logger.warning('Warning message...')
logger.error('Error message...')

logging.config.fileConfig(fname='logging.conf') # also possible to get config from dict with .dictConfig
simple_logger = logging.getLogger('simpleExample')
simple_logger.debug('This is a debug message')
import traceback


# try:
#     list_a = [1, 2, 3]
#     val = list_a[5]
# except IndexError as e:
#     logging.error(e, exc_info=True)

# # log without capturing a specific exception
# try:
#     list_a = [1, 2, 3]
#     val = list_a[5]
# except:
#     logging.error("The error is %s", traceback.format_exc())


#rotating file handlers
from logging.handlers import RotatingFileHandler

logger1 = logging.getLogger('MyLogger')
logger1.setLevel(logging.INFO)

handler1 = RotatingFileHandler('app.log', maxBytes=2000, backupCount=3)
logger1.addHandler(handler1)

for i in range(1000):
    logger1.info('num' + str(i) + ': Hello World!')


# timed rotating file handlers
from logging.handlers import TimedRotatingFileHandler
import time
# s, m, h, d, midnight, w0 (Monday), w1 (Tuesday), ...
handler2 = TimedRotatingFileHandler('timed_test.log', when='s', interval=5, backupCount=5)
logger1.addHandler(handler2)

for j in range(6):
    logger1.info('num' + str(j) + ': Timed Hello World!')
    time.sleep(5)
