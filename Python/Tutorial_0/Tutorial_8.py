# Exceptions

# a = 5 + '10' # TypeError
# import somemodule # ModuleNotFoundError
# b = c #NameError: name 'c' is not defined
# f = open('somefile.txt') #FileNotFoundError
# list_a = [1, 2, 3]
# list_a.remove(4) #ValueError
# list_a[4] #IndexError
# my_dict = {'name': 'Max'}
# my_dict['age'] #KeyError

from typing import Any


x = -5
# if x < 0:
#     raise Exception('x should be positive')
# assert (x>=0), 'x is not positive' # AssertionError

try:
    a = 5 / 1
    b = a + '10'
except ZeroDivisionError as e:
    print(e)
except TypeError as e:
    print(e)
except Exception as e:
    print('There was an error: ' + str(e))
else:
    print('Everything is fine')
finally: # runs always
    print('Cleaning up...')


# Creating own exceptions

class ValueTooHighError(Exception):
    pass

class ValueTooSmallError(Exception):
    def __init__(self, message, value) -> None:
        self.message = message
        self.value = value

def test_value(x):
    if x > 100:
        raise ValueTooHighError('Value is too high')
    if x < 5:
        raise ValueTooSmallError('Value is too small', x)
    


try:
    test_value(3)
except ValueTooHighError as e:
    print(e)
except ValueTooSmallError as e:
    print(e.message, ':', e.value)
