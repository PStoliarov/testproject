# JSON
import json

person = {
    "name": "John",
    "age": 30,
    "city": "New York",
    "hasChildren": False,
    "titles": ["engineer", "programmer"]
}

# serialization
# dumps - here s stands for string
#personJSON = json.dumps(person, indent=5, separators=('; ', '= '), sort_keys=True) # not recommended to change separators
personJSON = json.dumps(person, indent=5, sort_keys=True)
print(personJSON)

with open('person.json', 'w') as file:
    json.dump(person, file, indent=5)

# deserialization
person_dictionary = json.loads(personJSON)
print(person_dictionary)

with open('person.json', 'r') as openedFile:
    person_dictionary1 = json.load(openedFile)
    print(person_dictionary1)


class User:

    def __init__(self, name, age) -> None:
        self.name = name
        self.age = age


def encode_user(obj):
    if isinstance(obj, User):
        return {'name': obj.name, "age": obj.age, obj.__class__.__name__: True}
    else:
        raise TypeError('Object of type User is not JSON serializable')


user = User('Max', 27)
userJSON = json.dumps(user, default=encode_user)
print(userJSON)

# another way to encode custom data
from json import JSONEncoder

class UserEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, User):
            return {'name': obj.name, "age": obj.age, obj.__class__.__name__: True}
        else:
            return JSONEncoder.default(self, obj)
        
userJSON1 = json.dumps(user, cls=UserEncoder)
print(userJSON1)

# also works
userJSON2 = UserEncoder().encode(o=user)
print(userJSON2)

userPython_dict = json.loads(userJSON)
print(type(userPython_dict)) # <class 'dict'>


# custom decoding method
def decode_user(dct):
    if User.__name__ in dct:
        return User(name=dct['name'], age=dct['age'])
    return dct

userPython_obj = json.loads(userJSON, object_hook=decode_user)
print(type(userPython_obj)) # <class '__main__.User'>
print(userPython_obj.name)