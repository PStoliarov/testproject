# Strings: ordered, immutable, text representation
my_string1 = 'Hello World'
my_string2 = "I'm a programmer"
my_string3 = """Hello
World"""
print(my_string3)
my_string3 = """Hello \
World"""
print(my_string3)

char = my_string1[-1]
print(char)
#my_string1[0] = "h"  #TypeError: object does not support item assignment  -----  strings are immutable

substring = my_string1[:5]
print(substring)
reversed_string = my_string1[::-1]
print(reversed_string)

greeting = "Hello"
name = "Tom"

sentence = greeting + " " + name
print(sentence)

for i in greeting:
    print(i)

if 'el' in greeting:
    print("yes")
else:
    print("no")

my_string4 = '    Hello World     '
my_string4 = my_string4.strip()
print(my_string4)
print(my_string4.upper())
print(my_string4.lower())
print(my_string4.startswith('Hello')) # True
print(my_string4.endswith('World')) # True
print(my_string4.find('lo')) # 3
print(my_string4.find('pp')) # -1
print(my_string4.count('o')) # 2
print(my_string4.count('p')) # 0
print(my_string4.replace('World', 'Universe')) # original string is not changed


my_string5 = 'how are you doing'
my_list_characters = list(my_string5)
print(my_list_characters)

my_list_words = my_string5.split()
print(my_list_words)

my_string6 = 'how,are,you,doing'
my_list_words2 = my_string6.split(",")
print(my_list_words2)

new_string = ' '.join(my_list_words)
print(new_string)


from timeit import default_timer as timer

my_list_a = ['a'] * 100000

# bad
start = timer()
my_string7 = ''
for i in my_list_a:
    my_string7 += i
stop = timer()
print(stop-start)

# good
start = timer()
my_string7 = ''.join(my_list_a)
stop = timer()
print(stop-start)

# -------------Formatting-------------
#%
var1 = "Tom"
my_string8 = "the variable is %s" %var1
print(my_string8)

var2 = 3
my_string9 = "the variable is %d" %var2
print(my_string9)

var3 = 3.14152830
my_string9 = "the variable is %.3f" %var3
print(my_string9)

#.format()
var4 = 3.14152830
var5 = 6
my_string10 = "the variable is {:.2f} and {}".format(var4, var5)
print(my_string10)

#f-Strings
my_string11 = f"the variable is {var4*2} and {var5}"
print(my_string11)