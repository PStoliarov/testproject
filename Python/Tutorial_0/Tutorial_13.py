# Generators, memory efficient when working with large data

def mygenerator():
    yield 3
    yield 2
    yield 1

g = mygenerator() # <generator object mygenerator at 0x00000215F13E8880>
print(g)

for i in g:
    print(i)


# value = next(g) # StopIteration exception - generator has already iterated through all yield statements

# generators cannot be rewound, there are 2 options:
# 1. Run the generator function again, restarting the generation: 
# g = mygenerator()
#
# 2. Use the itertools.tee() function to create a second version of your generator:
# import itertools
# y = mygenerator()
# y, y_backup = itertools.tee(y)
# for x in y:
#     print(x)
# for x in y_backup:
#     print(x)

# because we previously iterated through generator, next few lines of code are commented out, 
# to use them, comment of any previous code that iterates through generator

# print(sum(g)) # 6
# print(sorted(g)) # [1, 2, 3]

print('------------------------------------------')

def countdown(num):
    print('Starting')
    while num > 0:
        yield num
        num -= 1

cd = countdown(4)

value = next(cd) # prints 'Starting' and reaches the first 'yield' statement
print(value) # prints 4
print(next(cd)) # prints 3
print(next(cd)) # prints 2
print(next(cd)) # prints 1
# print(next(cd)) # StopIteration exception

print('------------------------------------------')


def firstn(n):
    nums = []
    num = 0
    while num < n:
        nums.append(num)
        num += 1
    return nums

def firstn_generator(n):
    num = 0
    while num < n:
        yield num
        num += 1

import sys

print(sys.getsizeof(firstn(10000)))                 # 85176 bytes
print(sys.getsizeof(firstn_generator(10000)))       # 200 bytes



print('------------------------------------------')


def fibonacci(limit):
    a, b = 0, 1
    while a < limit:
        yield a
        a, b = b, a + b

fib_gen = fibonacci(30)
for i in fib_gen:
    print(i)


print('------------------------------------------')


my_list = [i for i in range(10000) if i%2 == 0]
print(sys.getsizeof(my_list))                           # 41880 bytes

mygenerator_1 = (i for i in range(10000) if i%2 == 0)
print(sys.getsizeof(mygenerator_1))                     # 208 bytes
