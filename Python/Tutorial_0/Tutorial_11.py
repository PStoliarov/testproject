# Random numbers

# sudo-random numbers (pseudo-random)
import random

random_num1 = random.random()
print(random_num1)
random_num2 = random.uniform(1, 10)
print(random_num2)

random_num3 = random.randint(1, 10)
print(random_num3)

random_num4 = random.randrange(1, 10) # same as randint but upper bound is not included
print(random_num4)

random_num5 = random.normalvariate(0, 1) # mean=0, standard_deviation=1
print(random_num5)


print('------------------------------------------')


mylist = list("ABCDEFGH")
choice_a = random.choice(mylist)
print(choice_a)

sample_a = random.sample(mylist, 3) # picks unique elements
print(sample_a)

choice_b = random.choices(mylist, k=3) # same as sample, but can pick non-unique elements
print(choice_b)

random.shuffle(mylist) # list is modified
print(mylist)

random.seed(1)
print(random.random())
print(random.randint(1, 10))

random.seed(2)
print(random.random())
print(random.randint(1, 10))

# using the same seed as before
random.seed(1)
print(random.random())          # prints the same number as before
print(random.randint(1, 10))    # prints the same number as before
# using the same seed as before
random.seed(2)
print(random.random())          # prints the same number as before
print(random.randint(1, 10))    # prints the same number as before


print('------------------------------------------')


# for security purposes better to use secrets module
import secrets

random_num6 = secrets.randbelow(10)
print(random_num6)
random_num7 = secrets.randbits(k=3) # min=000, max=111, in binary
print(random_num7)

mylist_2 = list("ABCDEFGH")
choice_c = secrets.choice(mylist_2)


print('------------------------------------------')


# for working with arrays you use numpy module
import numpy as np
random_array1d = np.random.rand(3) # returns 1d array with 3 random elements (floats)
print(random_array1d)
print('======================')

random_array2d = np.random.rand(3, 3) # returns 2d array (matrix) 3x3 with random elements (floats)
print(random_array2d)
print('======================')

random_array3d = np.random.rand(3, 3, 3) # returns 3d array 3x3x3 (3 matrices 3x3) with random elements (floats)
print(random_array3d)
print('======================')

random_array4 = np.random.randint(low=0, high=10, size=3) # upper bound is excluded
print(random_array4)
print('======================')

random_array5_2d = random_array4 = np.random.randint(low=0, high=10, size=(3, 4)) # upper bound is excluded
print(random_array5_2d)
print('======================')

arr = np.array([[1,2,3], [4,5,6], [7,8,9]])
print(arr)
np.random.shuffle(arr) # shuffles array indexes, elements within that arrays are not shuffled
print(arr)

print('======================')

np.random.seed(1)
print(np.random.rand(2, 2))
np.random.seed(1)
print(np.random.rand(2, 2)) # produces the same result