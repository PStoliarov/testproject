# itertools: product, permutations, combinations, accumulate, groupby, and infinite iterators
from itertools import product
a = [1, 2, 3]
b = [4, 5, 6]

prod = product(a, b) # Cartesian product of iterables
print(list(prod))


a1 = [1, 2]
b1 = [3]

prod1 = product(a1, b1) # Cartesian product of iterables
prod1_repeat = product(a1, b1, repeat=2)
print(list(prod1))
print(list(prod1_repeat))


from itertools import permutations
a2 = [1, 2, 3]
permut = permutations(a2)
print(list(permut))

permut2 = permutations(a2, 2)
print(list(permut2))


from itertools import combinations, combinations_with_replacement
a3 = [1, 2, 3, 4]
comb = combinations(a3, r=2) # gets unique combinations, r=number of elements in a combination
print(list(comb))

comb_wr = combinations_with_replacement(a3, 2) # elements can create combinations with self
print(list(comb_wr))


from itertools import accumulate
import operator
a4 = [1, 2, 3, 4]
acc = accumulate(a4) # each next element = accumulation of self with previous elements
print(a4)
print(list(acc))

acc_mul = accumulate(a4, func=operator.mul)
print(a4)
print(list(acc_mul))

a5 = [1, 2, 5, 3, 4]
acc_max = accumulate(a5, func=max)
print(a5)
print(list(acc_max))


from itertools import groupby

def smaller_than_3(x):
    return x < 3

a6 = [1, 2, 3, 4]
group_obj = groupby(a6, key=smaller_than_3)
#group_obj = groupby(a6, key=lambda x: x < 3)  # gives the same result
for key, value in group_obj:
    print(key, list(value))



persons = [{'name': 'Tim', 'age': 25},
           {'name': 'Dan', 'age': 25},
           {'name': 'Lisa', 'age': 27},
           {'name': 'Claire', 'age': 28}
           ]

group_persons = groupby(persons, key=lambda x: x['age'])
for key1, value1 in group_persons:
    print(key1, list(value1))



from itertools import count, cycle, repeat
for i in count(start=10, step=5):
    print(i)
    if i == 100:
        break

a7 = [1, 2, 3]
sequence_len = len(a7)
cnt = sequence_len * 2 # limit to stop infinite count
current_run = 0
for i in cycle(a7):
    print(i)
    current_run += 1
    if current_run == cnt:
        break

print("------------")

for i in repeat(1, 5):
    print(i) # prints 1 five times