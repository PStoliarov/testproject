mylist = ["banana", "cherry", "apple"]
print(mylist)

mylist2 = list()
print(mylist2)

#Different types, duplicates
mylist3 = [5, True, "apple", "apple"]
mylist3.clear()


item = mylist[0] #banana
item = mylist[-1] #apple

for i in mylist:
    print(i)

if "banana" in mylist:
    print("is in list")
else:
    print("is not in list")


length = len(mylist)

mylist.append("lemon")
print(mylist)

mylist.insert(1, "blueberry")
print(mylist)

popped_item = mylist.pop()
print(popped_item)
print(mylist)


removed_item = mylist.remove("cherry")
mylist.reverse()
print(mylist)

new_list = sorted(mylist) #does not change original list

mylist.sort() #changes original list
print(mylist)


mylist4 = [0] * 5
mylist5 = [1, 2, 3, 4, 5]
mylist_concatenated = mylist4 + mylist5
print(mylist_concatenated)


mylist6 = [1, 2, 3, 4, 5, 6, 7, 8, 9]

list_a = mylist6[1:5] # last index is excluded
print(list_a)
list_b = mylist6[:6]
list_c = mylist6[3:]
list_d = mylist6[::2] # every 2nd item from beginning to end
list_e = mylist6[::-1] # reversed list


list_original = ["banana", "cherry", "apple"]
list_copy1 = list_original
list_copy1.append("lemon") # also modifies original list
print(list_copy1)
print(list_original)

list_copy2 = list_original.copy() # list_copy2 = list(list_original) or list_copy2 = list_original[:]
list_copy2.append("watermelon") # does not modify original list
print(list_copy2)
print(list_original)


list_numbers = [1, 2, 3, 4, 5, 6]
list_squared = [i*i for i in list_numbers] # expression, iterator
print(list_numbers)
print(list_squared)

