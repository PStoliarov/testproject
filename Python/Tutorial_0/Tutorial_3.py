# Sets: unordered, mutable, no duplicates
myset1 = {1, 2, 3, 1}
print(myset1) # prints only elements that are accepted, 4th element did not make it to set because its duplicate

myset2 = set([1, 2, 3])

myset3 = set("Hello")
print(myset3)

my_empty_set1 = {}
print(type(my_empty_set1)) #<class 'dict'>
my_empty_set2 = set()
print(type(my_empty_set2)) #<class 'set'>


myset4 = set()
myset4.add(1)
myset4.add(2)
myset4.add(3)

myset4.remove(3) # can raise KeyError if element does not exist
myset4.discard(3) # version of remove() that does not raise KeyError
print(myset4)
print(myset4.pop())

for i in myset4:
    print(i)

if 2 in myset4:
    print("Exists")


odds = {1, 3, 5, 7, 9}
evens = {0, 2, 4, 6, 8}
primes = {2, 3, 5, 7}

union = odds.union(evens)
print(union)

intersection = odds.intersection(primes)
print(intersection)

setA = {1, 2, 3, 4, 5, 6, 7, 8, 9}
setB = {1, 2, 3, 10, 11, 12}

diff = setA.difference(setB)
print(diff)

symetric_diff = setA.symmetric_difference(setB)
print(symetric_diff)

setA.update(setB) # modifies setA
print(setA)