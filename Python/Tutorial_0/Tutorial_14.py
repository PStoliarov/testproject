# Threading vs Multiprocessing

from multiprocessing import Process
import os
import time

def square_numbers(count, process_num):
    print("From process # " + str(process_num) + " in the list:")
    for i in range(count):
        mult = i * i
        print(mult)
        time.sleep(0.1)

func_arg = 5
processes_list = []
num_processes = os.cpu_count() #16

for i in range(num_processes):
    p = Process(target=square_numbers, args=(func_arg, i)) # how to pass int?
    processes_list.append(p)

# start
for p in processes_list:
    if __name__ == '__main__': # to check for top-level environment ? without check: RuntimeError 
        # See also, https://stackoverflow.com/questions/72497140/in-python-multiprocessing-why-is-child-process-name-mp-main-is-there-a-way
        p.start()

# join
for p in processes_list:
    if __name__ == '__main__': # to check for top-level environment ? without check: RuntimeError 
        p.join()

print('End main')