#include "format_cpp_to_xml_gui.h"

Format_cpp_to_xml_gui::Format_cpp_to_xml_gui(QWidget *parent)
    : QMainWindow(parent)
{
    ui.setupUi(this);
    connect(ui.pushButton, SIGNAL(pressed()), this, SLOT(getFilePathFromDialog()));
    connect(ui.pushButton_2, SIGNAL(pressed()), this, SLOT(processHeader()));
}

Format_cpp_to_xml_gui::~Format_cpp_to_xml_gui()
{

}

void Format_cpp_to_xml_gui::getFilePathFromDialog()
{
    QString filePath = QFileDialog::getOpenFileName(this, "Open File", "C://", tr("Header file (*.h)"));
    if (!filePath.isEmpty())
    {
        ui.lineEdit->setText(filePath);
    }
}

void Format_cpp_to_xml_gui::processHeader()
{
    QString filePath = ui.lineEdit->text();
    QPalette palette = ui.label_status->palette();
    if (filePath.isEmpty())
    {
        ui.label_status->setText("Path is empty!");
        palette.setColor(ui.label_status->backgroundRole(), Qt::red);
        palette.setColor(ui.label_status->foregroundRole(), Qt::red);
        ui.label_status->setPalette(palette);
        return;
    }
    QFile file(filePath);
    if (!file.open(QIODevice::ReadOnly)) {
        ui.label_status->setText("Failed to open file!");
        palette.setColor(ui.label_status->backgroundRole(), Qt::red);
        palette.setColor(ui.label_status->foregroundRole(), Qt::red);
        ui.label_status->setPalette(palette);
        return;
    }
    if (ui.checkBox->isChecked())
    {
        QString fileName = file.fileName();
        file.copy(fileName + "_backup");
    }
    ui.textBrowser_output->setText("");
    QTextStream stream(&file);

    bool bCommentProcessed = false;
    QString cpp_details_start("/** \\details");
    QString cpp_details_end("*/");
    QString xml_comment;

    while (!stream.atEnd())
    {
        bCommentProcessed = false;
        QString line = stream.readLine();
        if (line.contains(QString("/** \\details")))
        {
            QString indentation = getIndentation(line);
            QString detailsSectionXMLStr("///<summary>");
            line.replace(cpp_details_start, detailsSectionXMLStr);
            ui.textBrowser_output->append(line);
            SectionRes res = kDetails;
            while (!bCommentProcessed)
            {
                line = stream.readLine();
                processNextSection(line, res, indentation);
                if (res == kDetails && !line.contains(cpp_details_end))
                {
                    line = indentation + "///" + line.simplified();
                }
                if (line.contains(cpp_details_end))
                {
                    bCommentProcessed = true;
                    //line.replace(cpp_details_end, "///</summary>");
                }
                if (line.contains(cpp_details_end) && res == kDetails)
                {
                    line.replace(cpp_details_end, "///</summary>");
                    ui.textBrowser_output->append(line);
                }
                if (!bCommentProcessed)
                {
                    ui.textBrowser_output->append(line);
                }
            }
        }
        if (bCommentProcessed)
        {
            continue;
        }
        ui.textBrowser_output->append(line);
        //ui.textBrowser_output->append("\n");
    }
    file.close();
}



void Format_cpp_to_xml_gui::processNextSection(QString& inputLine, SectionRes& res, QString& indentation)
{
    QString cpp_param("\\param");
    QString cpp_remarks("\\remarks");
    QString cpp_returns("\\returns");
    if (inputLine.contains(cpp_param))
    {
        processParam(inputLine, res, indentation);
        res = SectionRes::kParam;
    }

    if (inputLine.contains(cpp_remarks))
    {
        processRemarks(inputLine, res, indentation);
        res = SectionRes::kRemarks;
    }

    if (inputLine.contains(cpp_returns))
    {
        processReturns(inputLine, res, indentation);
        res = SectionRes::kReturns;
    }
}

void Format_cpp_to_xml_gui::processParam(QString& inputLine, SectionRes currentSection, QString& indentation)
{
    QString prevSectionEnding = endOfPreviousSection(currentSection);
    prevSectionEnding = indentation + prevSectionEnding;
    QString temp = inputLine;
    QRegExp rx("\\param\\s+(\\S+)\\s+(\\S+)\\s+(.+$)");
    int pos = rx.indexIn(temp);
    QString paramName, in_out, paramDesc;
    if (pos > -1) 
    {
        paramName = rx.cap(1);
        in_out = rx.cap(2);
        paramDesc = rx.cap(3);
    }
    if (!paramName.isEmpty() && !in_out.isEmpty() && !paramDesc.isEmpty())
    {
        temp = "///<param name=\"" + paramName + "\">" + " " + in_out + " " + paramDesc + "</param>";
    }
    inputLine = prevSectionEnding + indentation + temp;
}

void Format_cpp_to_xml_gui::processRemarks(QString& inputLine, SectionRes currentSection, QString& indentation)
{

}

void Format_cpp_to_xml_gui::processReturns(QString& inputLine, SectionRes currentSection, QString& indentation)
{

}

QString Format_cpp_to_xml_gui::endOfPreviousSection(SectionRes currentSection)
{
    QString prevSectionEnding;
    switch (currentSection)
    {
    case Format_cpp_to_xml_gui::kDetails:
        prevSectionEnding = "///</summary>\n";
        break;
    case Format_cpp_to_xml_gui::kParam:
        break;
    case Format_cpp_to_xml_gui::kRemarks:
        prevSectionEnding = "///</remarks>\n";
        break;
    case Format_cpp_to_xml_gui::kReturns:
        prevSectionEnding = "///</returns>\n";
        break;
    default:
        break;
    }
    return prevSectionEnding;
}

QString Format_cpp_to_xml_gui::getIndentation(QString& inputLine)
{
    QString res = "";
    int cnt = inputLine.size() - 1;
    for (int i = 0; i < cnt; i++)
    {
        if (!inputLine.at(i).isSpace()) 
        {
            break;
        }
        res.append(" ");
    }
    return res;
}
