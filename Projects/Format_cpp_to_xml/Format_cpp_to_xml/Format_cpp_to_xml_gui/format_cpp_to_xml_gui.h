#ifndef FORMAT_CPP_TO_XML_GUI_H
#define FORMAT_CPP_TO_XML_GUI_H

#include <QtWidgets/QMainWindow>
#include "ui_format_cpp_to_xml_gui.h"
#include <QFileDialog>
#include <QString>
#include <QFile>
#include <QTextStream>
#include <QRegExp>

class Format_cpp_to_xml_gui : public QMainWindow
{
    Q_OBJECT

public:
    Format_cpp_to_xml_gui(QWidget *parent = 0);
    ~Format_cpp_to_xml_gui();
    
    enum SectionRes
    {
        kDetails,
        kParam,
        kRemarks,
        kReturns,
    };
    void processNextSection(QString& inputLine, SectionRes& res, QString& indentation);
    void processParam(QString& inputLine, SectionRes currentSection, QString& indentation);
    void processRemarks(QString& inputLine, SectionRes currentSection, QString& indentation);
    void processReturns(QString& inputLine, SectionRes currentSection, QString& indentation);
    QString endOfPreviousSection(SectionRes currentSection);
    QString getIndentation(QString& inputLine);

private:
    Ui::Format_cpp_to_xml_guiClass ui;

    public slots:;
    void getFilePathFromDialog();
    void processHeader();
};

#endif // FORMAT_CPP_TO_XML_GUI_H
