#include <iostream>
#include <regex>
#include <fstream>
#include <string>
#include <Windows.h>
#include <lmcons.h>
#include <WinBase.h>
#include <stdio.h>

std::vector<std::string> UserNames;
std::vector<std::string> ProjectNames;
std::vector<std::string> StartPages;

void init()
{
    UserNames = { "apobeda", "apolyakov", "azaytsev", "pstoliarov", "stikhomirov"};

    ProjectNames = { "TA", "BimNv", "BimRv", "TC", "TDrawings", "TDNET",
        "TeighaNetClassic", "kDrawingsX", "FModeler", "IFC", "TDJava", "TKernel",
        "PRC", "PRCNET", "Publish", "TVisualize", "TCloud" };

    StartPages = { "TA_whats_new", "bimnv_whats_new", "tbim_new", "TC_whats_new", "tdrawings_whats_new", "tnet_Version",
        "tdnet_classic_whats_new", "kDrawingsX", "fm_whats_new", "tifc_whats_new", "TJAVA_Version", "tkernel_whats_new",
        "tprc_whats_new", "tprcn_whats_new", "tpublish_whats_new", "tv_whats_new", "tcloud_whats_new" };
}

/** \details
Retrieves user name and project name form the command line argument string.
*/
int checkArgument(std::string& uName, std::string& projName, std::string& argument)
{
    int indexName, indexProj = -1;
    for (int i = 0; i < UserNames.size(); i++)
    {
        indexName = argument.find(UserNames[i]);
        if (indexName != -1)
        {
            uName = UserNames[i];
            for (int j = 0; j < ProjectNames.size(); j++)
            {
                indexProj = argument.find(ProjectNames[j]);
                if (indexProj != -1)
                {
                    projName = ProjectNames[j];
                    return 0;
                }
            }
        }
    }
    return -1;
}

struct Fixer
{
    /** \details
    Sets up main fields of this struct.
    */
    void setupFixer(std::string user, std::string project);

    /** \details
    Method that processes html output.
    */
    void processHtml();
    
    /** \details
    Not needed for now.
    */
    bool isContentEmpty(std::string cont);
    
    /** \details
    Fills m_contentHtml string with content of unmodified frames.html.
    */
    void fillContentHtml(std::fstream &file);
    
    /** \details
    Modified content of m_contentHtml field. The argument fstream &file is not needed anymore.
    */
    void replaceContentHtml(std::fstream &file);

    /** \details
    Retrieves page index in a global vector of start pages.
    */
    unsigned int getPageIndex();

    std::string m_contentHtml;
    std::string m_pathHtml;
    std::string m_userName;
    std::string m_projectName;
};

void Fixer::setupFixer(std::string user, std::string project)
{
    m_projectName = project;
    m_userName = user;
    m_pathHtml = "C:/Documentation/" + m_userName + "/doc/Docomatic/Output/" + m_projectName + "/html/frames.html";
}

void Fixer::fillContentHtml(std::fstream &file)
{
    m_contentHtml = "";
    std::string temp = "";
    while (!file.eof())
    {
        std::getline(file, temp);
        temp += "\n";
        m_contentHtml += temp;
    }
}

void Fixer::replaceContentHtml(std::fstream &file)
{
    bool bMatched = false;

    std::regex e("!!SYMREF.html");
    bMatched = std::regex_search(m_contentHtml, e);
    if (bMatched)
        m_contentHtml = std::regex_replace(m_contentHtml, e, StartPages[getPageIndex()] + ".html");
}


unsigned int Fixer::getPageIndex()
{
    unsigned int index = 0;
    for (int i = 0; i < ProjectNames.size(); i++)
    {
        if (ProjectNames[i].compare(m_projectName) == 0)
        {
            index = i;
            break;
        }
    }
    return index;
}

void Fixer::processHtml()
{
    std::fstream htmlFile;
    htmlFile.open(m_pathHtml);
    if (!htmlFile.is_open())
    {
        std::cout << "Error opening the target HTML file\n";
        exit(EXIT_FAILURE);
    }
    else
    {
        std::cout << "\nFile is opened!\n" << std::endl;
        fillContentHtml(htmlFile);

        std::fstream copyFile;
        copyFile.open(m_pathHtml + "_copy", std::fstream::out);

        if (!copyFile.is_open())
        {
            std::cout << "Error opening the copied file for HTML\n";
            exit(EXIT_FAILURE);
        }
        else
        {
            // backup original content before modifying
            copyFile << m_contentHtml;
            replaceContentHtml(htmlFile);

            htmlFile.close();
            htmlFile.open(m_pathHtml, std::fstream::out);
            htmlFile << m_contentHtml;
        }
        copyFile.close();
    }
    htmlFile.close();

    std::cout << "Processed HTML successfully!\n";
}

int main(int argc, char *argv[])
{
    if (argc != 5)
    {
        std::cout << "Wrong number of arguments" << std::endl;
        return -1;
    }

    Fixer fixer;
    init();

    std::string projName;
    std::string uName;
    std::string buildStepStr = argv[2];
    int buildStep = std::stoi(buildStepStr);
    if (buildStep != 40) 
    {
        return -1;
    }
    std::string argument = argv[4];

    //example of argument argv[4] ----- std::string argument = "C:\\Documentation\\pstoliarov\\doc\\Docomatic\\Projects\\TVisualize\\TVisualize.dox";

    int val = checkArgument(uName, projName, argument);

    if (val == -1) 
    {
        std::cout << "project or user name is empty!" << std::endl;
        return -1;
    }

    fixer.setupFixer(uName, projName);
    fixer.processHtml();
    
    return 0;
}
