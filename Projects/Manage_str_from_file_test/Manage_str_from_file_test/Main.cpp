﻿#include <iostream>
#include <regex>
#include <fstream>
#include <string>
#include <stdlib.h>
#include <Windows.h>
#include <lmcons.h>

std::vector<std::string> UserNames;
std::vector<std::string> ProjectNames;
std::vector<std::string> StartPages;

void init()
{
    
    UserNames = { "pstoliarov", "azaytsev", "apobeda", "apolyakov", "stihkomirov"};
    

    ProjectNames = { "TA", "BimNv", "BimRv", "TC", "TDrawings", "TDNET",
        "kDrawingsNetClassic", "kDrawingsX", "FModeler", "IFC", "TDJava", "TKernel",
        "PRC", "PRCNET", "Publish", "TVisualize", "TCloud" };

	StartPages = { "TA_whats_new", "bimnv_whats_new", "tbim_new", "TC_whats_new", "tdrawings_whats_new", "tnet_Version",
        "tdnet_classic_whats_new", "kDrawingsX", "fm_whats_new", "tifc_whats_new", "TJAVA_Version", "tkernel_whats_new",
        "tprc_whats_new", "tprcn_whats_new", "tpublish_whats_new", "tv_whats_new", "tcloud_whats_new" };
}

void checkUserName(std::string& name)
{
    bool bMatched = false;

    std::regex e("(\.)");   // matches content inside title
    bMatched = std::regex_search(name, e);
    if (bMatched)
        name = std::regex_replace(name, e, "");
}


struct Fixer
{

    enum User
    {
        kPstoliarov = 0,
        kAzaytsev = 1,
        kApobeda = 2,
        kApolyakov = 3,
        kStihkomirov = 4,
    };

    enum Project
    {
        kArchitecture = 0,
        kBimNw = 1,
        kBimRv = 2,
        kCivil = 3,
        kDrawings = 4,
        kDrawingsNet = 5,
        kDrawingsNetClassic = 6,
        kDrawingsX = 7,
        kFacetModeler = 8,
        kIfc = 9,
        kJava = 10,
        kKernel = 11,
        kPrc = 12,
        kPrcNet = 13,
        kPublish = 14,
        kVisualize = 15,
        kWeb = 16
    };

    enum configurationType {
        kHtml = 0,
        kChm = 1
    };

    void Fixer::setupFixer(Fixer::User user, Fixer::Project project);

    void processHtml();
    void processChm();
    bool isContentEmpty(std::string cont);
    void fillContentHtml(std::fstream &file);
    void fillContentHhp(std::fstream &file);
    void replaceContentHtml(std::fstream &file);
    void replaceContentHhp(std::fstream &file);
    void launchChmCompiler();

    Fixer::Project getProject();
    Fixer::User getUser();

    std::string m_contentHtml;
    std::string m_contentHhp;
    std::string m_pathHtml;
    std::string m_pathHhp;
    std::string m_chmCompilerLocation;
    std::string m_chmCompilerArgument;
    Fixer::User m_user;
    Fixer::Project m_project;
};

void Fixer::setupFixer(Fixer::User user, Fixer::Project project)
{
    std::string projectName = ProjectNames[project];
    std::string userName = UserNames[user];
    m_project = project;
    m_user = user;
    m_pathHtml = "C:/Documentation/" + userName + "/doc/Docomatic/Output/" + projectName + "/html/frames.html";
    m_pathHhp = "C:/Documentation/" + userName + "/doc/Docomatic/Output/" + projectName + "/chm/" + projectName + ".hhp";



    m_chmCompilerLocation = "C:/Program Files (x86)/HTML Help Workshop/hhc.exe";
    m_chmCompilerArgument = "hhc " + m_pathHhp;
    /*
    // Begin implementation of user-independent path argument construction for chm compiler
    TCHAR pathString[_MAX_PATH];
    GetModuleFileName(NULL, pathString, _MAX_PATH);

    std::string pathStr = pathString; // Current path + .exe name

    std::regex e("\\Tools\\Manage_str_from_file_test\\.exe");
    pathStr = std::regex_replace(pathStr, e, ""); // path should be at Docomatic folder for each user
    

    m_chmCompilerArgument = pathStr + "/Output/" + projectName + "/chm/" + projectName + ".hhp";
    // End implementation of user-independent path argument construction for chm compiler
    */
}

bool Fixer::isContentEmpty(std::string cont)
{
    return cont.empty();
}

void Fixer::fillContentHtml(std::fstream &file)
{
    m_contentHtml = "";
    std::string temp = "";
    while (!file.eof())
    {
        std::getline(file, temp);
        temp += "\n";
        m_contentHtml += temp;
    }
}

void Fixer::fillContentHhp(std::fstream &file)
{
    m_contentHhp = "";
    std::string temp = "";
    while (!file.eof())
    {
        std::getline(file, temp);
        temp += "\n";
        m_contentHhp += temp;
    }
}

void Fixer::replaceContentHtml(std::fstream &file)
{
    bool bMatched = false;
    //std::string temp = m_contentHtml;

    std::regex e("!!SYMREF.html");   // matches content inside title
    bMatched = std::regex_search(m_contentHtml, e);
    if (bMatched)
        m_contentHtml = std::regex_replace(m_contentHtml, e, StartPages[getProject()] + ".html");
}

void Fixer::replaceContentHhp(std::fstream &file)
{
    bool bMatched0 = false;
    bool bMatched1 = false;
    
    std::string p0part1 = "\".+(!!SYMREF\.html)\",{3,4}";
    std::string p0part2 = UserNames[getUser()];
    std::string p0part3 = "\\doc\\Docomatic\\Output\\";
    std::string p0part4 = ProjectNames[getProject()];
    std::string p0part5 = "\\chm\\";
    std::string p0part6 = "!!SYMREF\.html\"";

    std::string pattern0 = p0part1;
    std::smatch m;
    std::regex e0(pattern0); // find long string part of which need to be replaced

    bMatched0 = std::regex_search(m_contentHhp, m, e0);
    if (bMatched0) 
    {
        std::string subString = m[0].str(); //retrieve matched long string

        std::regex e("!!SYMREF.html");
        subString = std::regex_replace(subString, e, StartPages[getProject()] + ".html"); // change !!SYMREF.html to {startpage}.html in the long string...
        m_contentHhp = std::regex_replace(m_contentHhp, e0, subString, std::regex_constants::match_flag_type::format_first_only); // insert modified long string
    }
    
    //Default topic = C:\Documentation\pstoliarov\doc\Docomatic\Output\TVisualize\chm\!!SYMREF.html
    std::string p1part1 = "Default topic.+";
    std::string p1part2 = UserNames[getUser()];
    std::string p1part3 = "\\doc\\Docomatic\\Output\\";
    std::string p1part4 = ProjectNames[getProject()];
    std::string p1part5 = "\\chm\\";
    std::string p1part6 = "\!\!SYMREF\.html";

    std::string pattern1 = p1part1;

    std::regex e1(pattern1); // find string with default topic
    bMatched1 = std::regex_search(m_contentHhp, e1);
    if (bMatched1)
    {
        std::string replacement1 = "Default topic=C:\\Documentation\\" + p1part2 + p1part3 + p1part4 + p1part5 + StartPages[getProject()] + ".html";
        m_contentHhp = std::regex_replace(m_contentHhp, e1, replacement1, std::regex_constants::match_flag_type::format_default);
    }

}

void Fixer::launchChmCompiler()
{
    STARTUPINFO startInfo = { 0 };
    PROCESS_INFORMATION processInfo = { 0 };
    std::string processArgument = "C:\\Program Files (x86)\\HTML Help Workshop\\hhc.exe ";

    size_t len = m_chmCompilerArgument.length();
    LPSTR commandLineArgument = new char[len + 1];
    m_chmCompilerArgument._Copy_s(commandLineArgument, len, len);
    commandLineArgument[len] = '\0';

    BOOL bSuccess = CreateProcess(TEXT(processArgument.c_str()), commandLineArgument, NULL, NULL, FALSE,
        NULL, NULL, NULL, &startInfo, &processInfo);

    //BOOL bSuccess = CreateProcess(TEXT("C:\\Program Files (x86)\\HTML Help Workshop\\HelloWorld.exe"), " Haiii", NULL, NULL, FALSE,
    //    NULL, NULL, NULL, &startInfo, &processInfo); // TODO: REMOVE

    if (bSuccess)
    {
        std::cout << "Process started\n";
        std::cout << "Process ID:\t" << processInfo.dwProcessId << std::endl;
        WaitForSingleObject(processInfo.hProcess, INFINITE);
    }
    else
    {
        std::cout << "Error to start the process: " << GetLastError() << std::endl;
    }
}

Fixer::Project Fixer::getProject()
{
    return m_project;
}

Fixer::User Fixer::getUser()
{
    return m_user;
}

void Fixer::processChm()
{

    std::fstream hhpFile;
    //m_pathHhp = "TVisualize - Copy.hhp"; // TODO: Remove later
    hhpFile.open(m_pathHhp);
    if (!hhpFile.is_open())
    {
        std::cout << "Error opening the target HHP(CHM) file\n";
        system("Pause");
        exit(EXIT_FAILURE);
    }
    else
    {
        std::cout << "\nFile is opened!\n" << std::endl;
        fillContentHhp(hhpFile);

        std::fstream copyFile;
        copyFile.open(m_pathHhp + "_copy", std::fstream::out);

        if (!copyFile.is_open())
        {
            std::cout << "Error opening the copied file for HHP(CHM)\n";
            system("Pause");
            exit(EXIT_FAILURE);
        }
        else
        {
            // backup original and replace in original
            copyFile << m_contentHhp;
            //replaceContentHhp(hhpFile); //TODO: uncomment
            hhpFile.close();
            hhpFile.open(m_pathHhp, std::fstream::out);
            hhpFile << m_contentHhp;
        }
        copyFile.close();
    }
    hhpFile.close();


    //

    launchChmCompiler();
    std::cout << "Processed HHP(CHM) successfully!\n";
}

void Fixer::processHtml()
{
    std::fstream htmlFile;
    htmlFile.open(m_pathHtml);
    if (!htmlFile.is_open())
    {
        std::cout << "Error opening the target HTML file\n";
        system("Pause");
        exit(EXIT_FAILURE);
    }
    else
    {
        std::cout << "\nFile is opened!\n" << std::endl;
        fillContentHtml(htmlFile);

        std::fstream copyFile;
        copyFile.open(m_pathHtml + "_copy", std::fstream::out);

        if (!copyFile.is_open())
        {
            std::cout << "Error opening the copied file for HTML\n";
            system("Pause");
            exit(EXIT_FAILURE);
        }
        else
        {
            // backup original and replace in original
            copyFile << m_contentHtml;
            replaceContentHtml(htmlFile);
            htmlFile.close();
            htmlFile.open(m_pathHtml, std::fstream::out);
            htmlFile << m_contentHtml;
        }
        copyFile.close();
    }
    htmlFile.close();

    std::cout << "Processed HTML successfully!\n";
}

int main()
{
    int inputProject;
    int inputUser;
    Fixer fixer;

    // Commented section may not be needed in future
    /*
    TCHAR userName[UNLEN + 1];
    DWORD userNameLen = UNLEN + 1;
    GetUserName((TCHAR*)userName, &userNameLen);
    std::string uName = userName;
    uName += ".DOCOMATI"; //TODO: remove later
    checkUserName(uName);
    */

    init();

    while (true)
    {
        
        std::cout << "----------------------------------";
        std::cout << "\nUsers:\n";
        for (int i = 0; i < UserNames.capacity(); i++)
        {
            std::cout << i << ". " << UserNames[i] << std::endl;
        }
        std::cout << UserNames.capacity() << ". Exit\n\n";
        std::cout << "Enter user:";
        std::cin >> inputUser;


        if (inputUser == UserNames.capacity())
            break;
        if (inputUser < 0 && inputUser > UserNames.capacity())
        {
            std::cout << "\nWrong value!" << std::endl;
            continue;
        }

        std::cout << "----------------------------------";
        std::cout << "\nProjects:\n";
        for (int i = 0; i < ProjectNames.capacity(); i++)
        {
            std::cout << i << ". " << ProjectNames[i] << std::endl;
        }
        std::cout << ProjectNames.capacity() << ". Exit\n\n";
        std::cout << "Enter project:";
        std::cin >> inputProject;

        if(inputProject == ProjectNames.capacity())
            break;
        if (inputProject > 0 && inputProject < ProjectNames.capacity())
        {
            fixer.setupFixer((Fixer::User)inputUser,(Fixer::Project)inputProject);

            int conf;

            std::cout << "\nConfigurations:\n";
            std::cout << "\n0. HTML\n";
            std::cout << "\n1. CHM\n";
            std::cout << "\n2. HTML + CHM\n";

            std::cout << "\nChoose configuration(s) to fix: ";
            std::cin >> conf;

            switch (conf)
            {
            case 0: 
            {
                fixer.processHtml();
                break;
            }

            case 1: 
            {
                fixer.processChm();
                break;
            }
            case 2:
            {
                fixer.processHtml();
                fixer.processChm();
            }
            default:
            {
                std::cout << "\nWrong value!\n";
                break;
            }

            }//switch

            
        }
        
        else
        {
            continue;
        }
    }

    return 0;
}


/*
std::cout << "----------------------------------";
std::cout << "\nUsers:\n";
for (int i = 0; i < UserNames.capacity(); i++)
{
std::cout << i << ". " << UserNames[i] << std::endl;
}
std::cout << UserNames.capacity() << ". Exit\n\n";
std::cout << "Enter user:";
std::cin >> inputUser;


if (inputUser == UserNames.capacity())
break;
if (inputUser < 0 && inputUser > UserNames.capacity())
{
continue;
}



void Fixer::setupFixer(std::string user, Fixer::Project project)
{
    std::string projectName = ProjectNames[project];
    m_pathHtml = "../Docomatic/Output/" + projectName + "/html/frames.html";
    m_pathHhp = "../Docomatic/Output/" + projectName + "/chm/" + projectName + ".hhp";

    m_replacementStrHtml = StartPages[project];
    m_replacementStrChm = StartPages[project];
    m_chmCompilerLocation = "C:/Program Files (x86)/HTML Help Workshop/hhc.exe";

    // Begin implementation of user-independent path argument construction for chm compiler
    TCHAR pathString[_MAX_PATH];
    GetModuleFileName(NULL, pathString, _MAX_PATH);

    std::string pathStr = pathString; // Current path + .exe name

    std::regex e("\Tools\Manage_str_from_file_test.exe");
    pathStr = std::regex_replace(pathStr, e, ""); // path should be at Docomatic folder for each user


    m_chmCompilerArgument = pathStr + "/Output/" + projectName + "/chm/" + projectName + ".hhp";
    // End implementation of user-independent path argument construction for chm compiler
    m_project = project;
}

*/