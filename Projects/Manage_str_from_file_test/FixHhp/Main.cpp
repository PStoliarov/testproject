#include <iostream>
#include <regex>
#include <fstream>
#include <string>
#include <stdlib.h>
#include <Windows.h>
#include <lmcons.h>
#include <WinBase.h>

std::vector<std::string> UserNames;
std::vector<std::string> ProjectNames;
std::vector<std::string> StartPages;

void init()
{
    UserNames = { "apobeda", "apolyakov", "azaytsev", "pstoliarov", "stikhomirov" };

    ProjectNames = { "TA", "BimNv", "BimRv", "TC", "TDrawings", "TDNET",
        "kDrawingsNetClassic", "kDrawingsX", "FModeler", "IFC", "TDJava", "TKernel",
        "PRC", "PRCNET", "Publish", "TVisualize", "TCloud" };

    StartPages = { "TA_whats_new", "bimnv_whats_new", "tbim_new", "TC_whats_new", "tdrawings_whats_new", "tnet_Version",
        "tdnet_classic_whats_new", "kDrawingsX", "fm_whats_new", "tifc_whats_new", "TJAVA_Version", "tkernel_whats_new",
        "tprc_whats_new", "tprcn_whats_new", "tpublish_whats_new", "tv_whats_new", "tcloud_whats_new" };
}

/** \details
Retrieves user name and project name form the command line argument string.
*/
int checkArgument(std::string& uName, std::string& projName, std::string& argument)
{
    int indexName, indexProj = -1;
    for (int i = 0; i < UserNames.size(); i++)
    {
        indexName = argument.find(UserNames[i]);
        if (indexName != -1)
        {
            uName = UserNames[i];
            for (int j = 0; j < ProjectNames.size(); j++)
            {
                indexProj = argument.find(ProjectNames[j]);
                if (indexProj != -1)
                {
                    projName = ProjectNames[j];
                    return 0;
                }
            }
        }
    }
    return -1;
}


struct Fixer
{
    /** \details
    Sets up main fields of this struct.
    */
    void Fixer::setupFixer(std::string user, std::string project);

    /** \details
    Method that processes chm output.
    */
    void processChm();
    
    /** \details
    Fills m_contentHhp string with content of unmodified .hhp file.
    */
    void fillContentHhp(std::fstream &file);
    
    /** \details
    Modifies content of m_contentHhp field. The argument fstream &file is not needed anymore.
    */
    void replaceContentHhp(std::fstream &file);

    /** \details
    Retrieves page index in a global vector of start pages.
    */
    unsigned int getPageIndex();

    std::string m_contentHhp;
    std::string m_pathHhp;
    std::string m_userName;
    std::string m_projectName;
};

void Fixer::setupFixer(std::string user, std::string project)
{
    m_projectName = project;
    m_userName = user;
    m_pathHhp = "C:/Documentation/" + m_userName + "/doc/Docomatic/Output/" + m_projectName + "/chm/" + m_projectName + ".hhp";
}

void Fixer::fillContentHhp(std::fstream &file)
{
    m_contentHhp = "";
    std::string temp = "";
    while (!file.eof())
    {
        std::getline(file, temp);
        temp += "\n";
        m_contentHhp += temp;
    }
}

void Fixer::replaceContentHhp(std::fstream &file)
{
    bool bMatched0 = false;
    bool bMatched1 = false;

    std::string p0part1 = "\".+(!!SYMREF\.html)\",{3,4}";
    std::string p0part2 = m_userName;
    std::string p0part3 = "\\doc\\Docomatic\\Output\\";
    std::string p0part4 = m_projectName;
    std::string p0part5 = "\\chm\\";
    std::string p0part6 = "!!SYMREF\.html\"";

    std::string pattern0 = p0part1;
    std::smatch m;
    std::regex e0(pattern0); // find long string part of which need to be replaced

    bMatched0 = std::regex_search(m_contentHhp, m, e0);
    if (bMatched0)
    {
        std::string subString = m[0].str(); //retrieve matched long string

        std::regex e("!!SYMREF.html");
        subString = std::regex_replace(subString, e, StartPages[getPageIndex()] + ".html"); // change !!SYMREF.html to {startpage}.html in the long string...
        m_contentHhp = std::regex_replace(m_contentHhp, e0, subString, std::regex_constants::match_flag_type::format_first_only); // insert modified long string


        // part 2
        //Default topic = C:\Documentation\pstoliarov\doc\Docomatic\Output\TVisualize\chm\!!SYMREF.html
        std::string p1part1 = "Default topic.+";
        std::string p1part2 = m_userName;
        std::string p1part3 = "\\doc\\Docomatic\\Output\\";
        std::string p1part4 = m_projectName;
        std::string p1part5 = "\\chm\\";
        std::string p1part6 = "\!\!SYMREF\.html";

        std::string pattern1 = p1part1;

        std::regex e1(pattern1); // find string with default topic
        bMatched1 = std::regex_search(m_contentHhp, e1);
        if (bMatched1)
        {
            std::string replacement1 = "Default topic=C:\\Documentation\\" + p1part2 + p1part3 + p1part4 + p1part5 + StartPages[getPageIndex()] + ".html";
            m_contentHhp = std::regex_replace(m_contentHhp, e1, replacement1, std::regex_constants::match_flag_type::format_default);
        }

    }
}


unsigned int Fixer::getPageIndex()
{
    unsigned int index = 0;
    for (int i = 0; i < ProjectNames.size(); i++)
    {
        if (ProjectNames[i].compare(m_projectName) == 0)
        {
            index = i;
            break;
        }
    }
    return index;
}

void Fixer::processChm()
{
    std::fstream hhpFile;
    hhpFile.open(m_pathHhp);
    if (!hhpFile.is_open())
    {
        std::cout << "Error opening the target HHP file\n";
        exit(EXIT_FAILURE);
    }
    else
    {
        std::cout << "\nFile is opened!\n" << std::endl;
        fillContentHhp(hhpFile);

        std::fstream copyFile;
        copyFile.open(m_pathHhp + "_copy", std::fstream::out);

        if (!copyFile.is_open())
        {
            std::cout << "Error opening the copied file for HHP\n";
            exit(EXIT_FAILURE);
        }
        else
        {
            // backup original content before modifying
            copyFile << m_contentHhp;
            replaceContentHhp(hhpFile);

            hhpFile.close();
            hhpFile.open(m_pathHhp, std::fstream::out);
            hhpFile << m_contentHhp;
        }
        copyFile.close();
    }
    hhpFile.close();

    std::cout << "Processed CHM successfully!\n";
}

int main(int argc, char *argv[])
{
    if (argc != 5) 
    {
        std::cout << "Wrong number of arguments" << std::endl;
        return -1;
    }
    Fixer fixer;
    init();

    std::string projName;
    std::string uName;
    std::string buildStepStr = argv[2];
    int buildStep = std::stoi(buildStepStr);
    if (buildStep != 40)
    {
        return -1;
    }
    std::string argument = argv[4];

    //example of argument argv[4] ----- std::string argument = "C:\\Documentation\\pstoliarov\\doc\\Docomatic\\Projects\\TVisualize\\TVisualize.dox";
    
    int val = checkArgument(uName, projName, argument);

    if (val == -1)
    {
        std::cout << "project or user name is empty!" << std::endl;
        return -1;
    }

    fixer.setupFixer(uName, projName);
    fixer.processChm();

    return 0;
}
